__author__ = 'Breinbaas'

#-------------------------------------------------------------------------------
# Name:        cpt
# Purpose:     Class for CPT (cone penetration test) reading, writing and
#              calculations
#
# Author:      Breinbaas
#
# Created:     21-09-2014
# Copyright:   (c) Breinbaas 2014
# Licence:     GPL
#
# TODO TESTCASES
#        r.qc_wg_to_soiltype_id(1, 0.1) #expect 20
#        r.qc_wg_to_soiltype_id(1, 1)
#        r.qc_wg_to_soiltype_id(1, 10)  #21
#        r.qc_wg_to_soiltype_id(2, 2) #22
#        r.qc_wg_to_soiltype_id(7, 0.8) #23
#        r.qc_wg_to_soiltype_id(20, 0.5) #24
#        r.qc_wg_to_soiltype_id(100, 0.5) #25
#        r.qc_wg_to_soiltype_id(1000, 0.5) #26
#        r.qc_wg_to_soiltype_id(1000, 2) #27
#        r.qc_wg_to_soiltype_id(1000, 8) #28
#        r.qc_wg_to_soiltype_id(30, 6) #29
#        r.qc_wg_to_soiltype_id(60, 6) #30
#        r.qc_wg_to_soiltype_id(180, 7) #31
#        r.qc_wg_to_soiltype_id(500, 7) #32
#-------------------------------------------------------------------------------

import sys, robertson
from collections import Counter

#helper function to mimic ENUM variables
def enum(**enums):
    return type('Enum', (), enums)

#different types of correlation based on CUR166 figures
CPT2SoilCorrelationTypes = enum(CUR166ELECTRICALCONE=1, CUR166MECHANICALCONE=2, ROBERTSON_NL=3)

#CUR rules are based on wrijvingsgetal > soilname. Rules should be decreasing!
CUR166ELECTRICALRULES = [[0.6,'grof zand (cur)',20], [0.8,'middelgrof zand (cur)',19],\
    [1.1,'fijn zand (cur)',18],[1.4,'siltig zand (cur)',17],[1.8,'kleiig zand (cur)',17],\
    [2.2,'zandige klei (cur)',16],[2.5,'siltige klei (cur)',16],[2.9,'klei (cur)',15],\
    [3.3,'slappe klei (cur)',14],[4.0,'venige klei (cur)',12],[8.1,'veen (cur)',10.5]
        ]
CUR166MECHANICALRULES = [[1.3,'grof zand (cur)',20],[1.6,'fijn zand (cur)',19],[2.3,'siltig zand (cur)',18],\
    [2.8,'kleiig zand (cur)',16],[3.1,'leem (cur)',15],[3.6,'zandige klei (cur)',15],\
    [5.1,'overgeconsolideerde klei (cur)',16],[6.3,'venige klei (cur)',14],[8.0,'veen (cur)',10.5]]

class CPT:
    def __init__(self):
        """A CPT (cone penetration test) contains information on subsoil parameters."""
        self.xy = [] #location in RD coordinates
        self.surfacelevel = 0.0 #surfacelevel of the cpt
        self.z = [] #values of depths
        self.qc = [] #values of cone resistance
        self.pw = [] #values of plaatselijke wrijving
        self.wg = [] #values of wrijvingsgetal
        self.uu = [] #values of waterspanning
        self.testid = "No name"

    def load(self, filename):
        """Load CPT from GEF file"""
        lines = open(filename,  'r').readlines()
        columnseperator, columninfo, columnvoid, index = self._readHeader(lines)
        self._readData(lines, index, columnseperator, columninfo, columnvoid)

    def _readHeader(self, lines):
        """Read the first part of the CPT which is a header containing metadata"""
        columninfo = []
        columnvoid = []
        columnseperator = ' '
        for i in range(0, len(lines)):
            if lines[i].find("#EOH")>-1:
                return columnseperator, columninfo, columnvoid, i+1

            id, argsline = lines[i].split('=')
            args = [a.strip() for a in argsline.split(',')] #strip all whitespaces front and rear

            if id=="#COLUMNINFO":
                columninfo.append([int(args[0]), int(args[3])])
            elif id=="#COLUMNSEPARATOR":
                columnseperator = args[1]
            elif id=="#COLUMNVOID":
                columnvoid.append(int(float(args[1])))
            elif id=="#XYID":
                self.xy = [float(args[1]), float(args[2])]
            elif id=="#ZID":
                self.surfacelevel = float(args[1])
            elif id=="#TESTID":
                self.testid = args[0]


    def _readData(self, lines, index, columnseperator, columninfo, columvoid):
        """Read the second part of the CPT which contains the measurement"""
        #determine the location of the columns with the needed data
        iz, iqc, ipw, iwg = -1, -1, -1, -1
        for c in columninfo:
            if c[1] == 1: #sondeerlengte
                iz = c[0] - 1 #zero based indexing
            elif c[1] == 2: #punt druk
                iqc = c[0] - 1
            elif c[1] == 3: #lokale wrijving
                ipw = c[0] - 1
            elif c[1] == 4: #wrijvings getal
                iwg = c[0] - 1
            elif c[1] == 11: #gec. diepte (maatgevend over sondeerlengte)
                iz = c[0] - 1

        #read and save data
        for i in range(index, len(lines)):
            args = [a.strip() for a in lines[i].split(columnseperator)]

            #make sure the line does not contain any columnvoid value, cpt will skip this incomplete data
            skip = float(args[iqc]) == columvoid[iqc]
            skip = skip or float(args[iz]) == columvoid[iz]
            skip = skip or float(args[ipw]) == columvoid[ipw]
            if iwg > -1:
                skip = skip or float(args[iwg]) == columvoid[iwg]

            if not skip:
                qc = float(args[iqc])
                if qc <= 0.0: qc = 1e-5
                pw = float(args[ipw])
                if pw < 1e-5: pw = 1e-5
                self.qc.append(qc)
                self.z.append(self.surfacelevel - abs(float(args[iz])))
                self.pw.append(pw)
                if iwg > 0:
                    wg = float(args[iwg])
                    if wg < 0.0: wg = 0.0
                    self.wg.append(wg)
                else:
                    self.wg.append(pw/qc * 100.0)

    def save(self):
        pass

    def bearing_capacity(self, pile):
        pass

    def plot(self, exportpath=""):
        """Plot the CPT using matplotlib
        save = bool, if true the plot is saved to the given exportpath,
        exportpath = path to export the plot to."""
        import matplotlib.pyplot as plt
        from matplotlib import gridspec
        from matplotlib import rc
        import os.path

        font = {'size': 10}
        rc('font', **font)
        plt.close('all')
        fig = plt.figure(figsize=(15,10))
        gs = gridspec.GridSpec(1, 3, width_ratios=[3, 1, 1])
        qcplot = fig.add_subplot(gs[0])
        qcplot.set_xlim([0,30])
        qcplot.set_title("conusweerstand [MPa]")
        qcplot.plot(self.qc, self.z)
        pwplot = fig.add_subplot(gs[1])
        pwplot.set_xlim([0,0.2])
        pwplot.set_title("plaatselijke wrijving [MPa]")
        pwplot.plot(self.pw, self.z)
        wgplot = fig.add_subplot(gs[2])
        wgplot.set_xlim([0,10])
        wgplot.set_title("wrijvingsgetal [%]")
        wgplot.plot(self.wg, self.z)
        gs.tight_layout(fig)
        plt.suptitle(self.testid)
        if exportpath!="":
            filename = os.path.join(exportpath, self.testid + ".png")
            plt.savefig(filename)
        else:
            plt.show()

    def to_soillayers(self, method=CPT2SoilCorrelationTypes.CUR166ELECTRICALCONE, minInterval=0.2):
        """Convert CPT to soillayers using a specified method
        method = one of the following;
        CUR166ELECTRICALCONE: CUR166 fig. A1b (Default)
        CUR166MECHANICALCONE: CUR166 fig. A1a
        ROBERTSON_NL: using the Robertson graph adjusted to Dutch soiltypes
        minInterval = minimum soillayer height (default 0.2m)
        """
        if method == CPT2SoilCorrelationTypes.CUR166ELECTRICALCONE:
            result = self._to_soillayers_cur(CUR166ELECTRICALRULES, minInterval)
        elif method == CPT2SoilCorrelationTypes.CUR166MECHANICALCONE:
            result = self._to_soillayers_cur(CUR166MECHANICALRULES, minInterval)
        elif method == CPT2SoilCorrelationTypes.ROBERTSON_NL:
            result = self._to_soillayers_robertson_nl(minInterval, 1.0)
        return result

    def _to_soillayers_robertson_nl(self, minInterval, assumedWaterLevelFromTop):
        """
        Convert cpt to soillayers based on the Robertson correlations. Due to
        the needed normalized qc and fs we assume the weight of the soil by
        first using the CUR166 electrical rule based on only the wrijvingsgetal.
        """
        r = robertson.Robertson() #define the correlation class
        soilids = [] #the intermediate result
        ytot = 0.0 #total weight of soil + water
        ywtot = 0.0 #total water weight
        zw = self.z[0] - assumedWaterLevelFromTop #from this z on we assume water
        for i in range(0, len(self.qc)):
            qc = self.qc[i]
            fs = self.pw[i]
            wg = self.wg[i]
            z = self.z[i]

            #special Dutch rule, check if this is peat
            if qc < 1.5 and wg > 5.0:
                soilids.append([z, "robertson_holoceen_veen"])
                if i==0:
                    dl = 0.01
                else:
                    dl = abs(self.z[i] - self.z[i-1])
                ytot += 10.5 * dl #add the weight of the soil
                if self.z[i] <= zw:
                    ywtot += dl * 10.0 #and the weight of the water
            else:
                #determine the soiltype based on CUR166 electrical rules
                grondsoort = self._rule_to_grondsoort(CUR166ELECTRICALRULES, wg)
                if i==0:
                    dl = 0.01
                else:
                    dl = abs(self.z[i] - self.z[i-1])
                ytot += dl * CUR166ELECTRICALRULES[grondsoort][2] #add the weight of the soil

                if self.z[i] <= zw:
                    ywtot += dl * 10.0 #add the weight of the water

                nqc = (qc * 1000 - ytot) / (ytot - ywtot) #normalize the qc
                nrf = (fs * 1000 / (qc * 1000 - ytot)) * 100 #normalize fs

                soilids.append([z, r.qc_wg_to_soiltype_id(nqc, nrf)])

        #make a list of soillayers using at least the minimum interval
        i = 0
        idsatinterval = []
        while i < len(soilids):
            z1 = soilids[i][0]
            ids = [soilids[i][1]]
            i += 1
            while i<len(soilids):
                z2 = soilids[i][0]
                ids.append(soilids[i][1])
                if (z1 - z2) > minInterval:
                    idsatinterval.append([z1, z2, Counter(ids).most_common(1)[0][0]])
                    break
                if z2 == self.z[-1]:
                    idsatinterval.append([z1, z2, Counter(ids).most_common(1)[0][0]])
                    break
                i += 1

        #now merge layers with the same id
        result = []

        if len(idsatinterval)==0:
            raise Exception("The interval is larger than z;max minus z;min.")

        top = idsatinterval[0][0]
        bottom = idsatinterval[0][1]
        curid = idsatinterval[0][2]
        for i in range(1, len(idsatinterval)):
            if i==len(idsatinterval)-1:
                bottom = idsatinterval[i][1]
                result.append([top, bottom, curid])
            if idsatinterval[i][2] != curid:
                result.append([top, bottom, curid])
                top = idsatinterval[i][0]
                bottom = idsatinterval[i][1]
                curid = idsatinterval[i][2]
        return result

    def _rule_to_grondsoort(self, rules, wg):
        for i in range(0, len(rules)):
            if i == len(rules)-1:
                return i
                break
            if wg <= rules[i][0]:
                return i

    def _to_soillayers_cur(self, rules, minInterval):
        """Convert CPT to soillayers using CUR166 fig. A1b"""
        #convert every measurement to a soiltype and save it in a list
        soilids = []
        for i in range(0, len(self.wg)):
            grondsoort = self._rule_to_grondsoort(rules, self.wg[i])
            soilids.append([self.z[i],grondsoort])

        #make a list of soillayers using at least the minimum interval
        i = 0
        idsatinterval = []
        while i < len(soilids):
            z1 = soilids[i][0]
            ids = [soilids[i][1]]
            i += 1
            while i<len(soilids):
                z2 = soilids[i][0]
                ids.append(soilids[i][1])
                if (z1 - z2) > minInterval:
                    idsatinterval.append([z1, z2, Counter(ids).most_common(1)[0][0]])
                    break
                if z2 == self.z[-1]:
                    idsatinterval.append([z1, z2, Counter(ids).most_common(1)[0][0]])
                    break
                i += 1

        #now merge layers with the same id
        result = []

        if len(idsatinterval)==0:
            raise Exception("The interval is larger than z;max minus z;min.")

        top = idsatinterval[0][0]
        bottom = idsatinterval[0][1]
        curid = idsatinterval[0][2]
        for i in range(1, len(idsatinterval)):
            if i==len(idsatinterval)-1:
                bottom = idsatinterval[i][1]
                result.append([top, bottom, rules[curid][1]])
            if idsatinterval[i][2] != curid:
                result.append([top, bottom, rules[curid][1]])
                top = idsatinterval[i][0]
                bottom = idsatinterval[i][1]
                curid = idsatinterval[i][2]

        return result

if __name__=="__main__":
    cpt = CPT()
    cpt.load("testdata/s01.gef")
    r = cpt.to_soillayers(CPT2SoilCorrelationTypes.CUR166ELECTRICALCONE )
    r2 = cpt.to_soillayers(CPT2SoilCorrelationTypes.ROBERTSON_NL)
    print r
    print r2

