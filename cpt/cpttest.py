__author__ = 'Breinbaas'

#-------------------------------------------------------------------------------
# Name:        cpttest
# Purpose:     test module for cpt.py
#
# Author:      Breinbaas
#
# Created:     21-09-2014
# Copyright:   (c) Breinbaas 2014
# Licence:     GPL
#-------------------------------------------------------------------------------

"""Unit test for cpt module"""

import unittest
from cpt import *

class CPTTester(unittest.TestCase):
    def testCPTLoad(self):
        """Loading and checking the cpt"""
        cpt = CPT()
        cpt.load("testdata/s01.gef")
        self.assertEqual(cpt.xy, [127496.0, 483738.0])
        self.assertEqual(cpt.surfacelevel, -0.637)

if __name__ == '__main__':
    unittest.main()
