#-------------------------------------------------------------------------------
# Name:        rdcoordinate
# Purpose:     Represents a coordinate in RD space
#
# Author:      Rob van Putten
#
# Created:     13-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

import math

import bbtools.gis.wgs84coordinate

class RDCoordinate:
    '''Class representing a (Dutch) Rijksdriehoek (RD) coordinate'''
    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y

    def to_wgs84(self):
        '''Convert to WGS84 coordinates'''
        result = bbtools.gis.wgs84coordinate.WGS84Coordinate()
        x0 = 155000
        y0 = 463000
        phi0  = 52.15517440
        lam0 = 5.38720621

        Kp = [0,2,0,2,0,2,1,4,2,4,1]
        Kq = [1,0,2,1,3,2,0,0,3,1,1]
        Kpq = [3235.65389,-32.58297,-0.24750,-0.84978,-0.06550,-0.01709,-0.00738,0.00530,-0.0039,0.00033,-0.00012]

        Lp = [1,1,1,3,1,3,0,3,1,0,2,5]
        Lq = [0,1,2,0,3,1,1,2,4,2,0,0]
        Lpq = [5260.52916,105.94684,2.45656,-0.81885,0.05594,-0.05607,0.01199,-0.00256,0.00128,0.00022,-0.00022,0.00026]

        dx = 1E-5 * ( self.x - x0 )
        dy = 1E-5 * ( self.y - y0 )

        for k in range(len(Kpq)):
            result.latitude = result.latitude + ( Kpq[k] * dx**Kp[k] * dy**Kq[k] )
        result.latitude = phi0 + result.latitude / 3600

        for l in range(len(Lpq)):
            result.longitude = result.longitude + ( Lpq[l] * dx**Lp[l] * dy**Lq[l] )
        result.longitude = lam0 + result.longitude / 3600

        return result

    def distance_to(self, tocoord):
        '''Calculate the distance to another RD coordinate.'''
        dx = tocoord.x - self.x
        dy = tocoord.y - self.y
        sum = dx*dx + dy*dy
        if sum > 0:
            return math.sqrt(dx*dx + dy*dy)
        else:
            return 0

if __name__=="__main__":
    rd = RDCoordinate(112113, 456340)
    print rd.to_wgs84()