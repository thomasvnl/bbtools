﻿#-------------------------------------------------------------------------------
# Name:        wgs84coordinate
# Purpose:     represents a coordinate in WGS84 space
#
# Author:      Rob van Putten
#
# Created:     13-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

import math

import bbtools.gis.rdcoordinate

class WGS84Coordinate:
    '''Class representing a WGS84 coordinate'''
    def __init__(self, latitude=0.0, longitude=0.0):
        self.latitude = latitude
        self.longitude = longitude

    def to_rd(self):
        '''Convert to (Dutch) Rijksdriehoek (RD) coordinates using the
        approx. method'''
        result = bbtools.gis.rdcoordinate.RDCoordinate(0, 0)

        x0 = 155000
        y0 = 463000
        phi0 = 52.15517440
        lam0 = 5.38720621

        Rp = [0,1,2,0,1,3,1,0,2]
        Rq = [1,1,1,3,0,1,3,2,3]
        Rpq = [190094.945,-11832.228,-114.221,-32.391,-0.705,-2.340,-0.608,-0.008,0.148]

        Sp = [1,0,2,1,3,0,2,1,0,1]
        Sq = [0,2,0,2,0,1,2,1,4,4]
        Spq = [309056.544,3638.893,73.077,-157.984,59.788,0.433,-6.439,-0.032,0.092,-0.054]

        dphi = 0.36 * ( self.latitude - phi0 )
        dlam = 0.36 * ( self.longitude - lam0 )

        for r in range( len( Rpq ) ):
            result.x = result.x + ( Rpq[r] * dphi**Rp[r] * dlam**Rq[r] )
        result.x += x0

        for s in range( len( Spq ) ):
            result.y = result.y + ( Spq[s] * dphi**Sp[s] * dlam**Sq[s] )
        result.y += y0

        return result

    def distance_to(self, topoint):
        '''Calculate the distance in km between 2 WGS84 points'''
        R = 6371.0
    	dlat = (topoint.latitude - self.latitude) / 180.0 * math.pi
    	dlon = (topoint.longitude - self.longitude) / 180.0 * math.pi
    	lat1 = self.latitude / 180.0 * math.pi
    	lat2 = topoint.latitude / 180.0 * math.pi
    	a = math.sin(dlat/2) * math.sin(dlat/2) + math.sin(dlon/2) * math.sin(dlon/2) * math.cos(lat1) * math.cos(lat2)
    	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    	return R * c

if __name__=="__main__":
    w = WGS84Coordinate(5.2, 43.5)
    print w.to_rd()

