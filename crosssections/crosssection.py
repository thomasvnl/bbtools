#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Waternet
#
# Created:     06-01-2015
# Copyright:   (c) Waternet 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class Crosssection:
    def __init__(self):
        self.points = []
        self.code = ""
        self.metrering = 0

    def get_refpoint(self):
        '''Returns the point of the referentielijn intersection'''
        for p in self.points:
            if p[0]==90:
                return p
        return None

