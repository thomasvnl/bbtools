#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Breinbaas
#
# Created:     07-01-2015
# Copyright:   (c) Breinbaas 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class Soiltype:
    def __init__(self):
        self.name = "" #must be a unique name
        self.description = ""
        self.source = ""
        self.ydry = 0.0
        self.ysat = 0.0
        self.c = 0.0
        self.phi = 0.0
        self.upsilon = 0.0
        self.k = 0.0
        self.MC_upsilon = 0.0
        self.MC_E50 = 0.0
        self.HS_E50 = 0.0
        self.HS_Eoed = 0.0
        self.HS_Eur = 0.0
        self.HS_m = 0.0
        self.SSC_lambda = 0.0
        self.SSC_kappa = 0.0
        self.SSC_mu = 0.0
        self.Cp = 0.0
        self.Cs = 0.0
        self.Cap = 0.0
        self.Cas = 0.0
        self.cv = 0.0
        self.color = ""
        self.a_abc = 0.0
        self.b_abc = 0.0
        self.c_abc = 0.0
        self.e0 = 0.0

