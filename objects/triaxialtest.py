#-------------------------------------------------------------------------------
# Name:        triaxialtest
# Purpose:     Class that stores the main information about a triaxial test
#
# Author:      Rob van Putten
#
# Created:     13-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

class TriaxialTest(self):
    def __init__(self):
        self.boring_id = ""
        self.monster_id = ""
        self.omschrijving = ""
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.latitude = 0.0
        self.longitude = 0.0
        self.mv_boring = 0.0
        self.ynat = 0.0
        self.watergehalte = 0.0
        self.c2 = 0.0
        self.phi2 = 0.0
        self.c5 = 0.0
        self.phi5 = 0.0
        self.maxrek = 0.0
        self.c_maxrek = 0.0
        self.phi_maxrek = 0.0
        self.grondsoort = "" #klei, veen, zand ...
