#-------------------------------------------------------------------------------
# Name:        vsoil
# Purpose:
#
# Author:      Rob van Putten
#
# Created:     22-12-2014
# Copyright:   (c) Breinbaas 2014
# Licence:     GPL
#-------------------------------------------------------------------------------

class VSoil:
    '''A Vsoil represents a stack of soillayers placed on top of each other.
    NOTE: The vsoil class members need to have the EXACT same name as the database
    columns.'''
    def __init__(self):
        self.id = 0
        self.x = 0.0
        self.y = 0.0
        self.latitude = 0.0
        self.longitude = 0.0
        self.source = ""
        self.data = ""
        self.soillayers = [] #list with [ztop, zbottom, soilname]
        self.name = ""
        self.levee_location = 0

    def get_soilname_at(self, y):
        '''find the soil at the given level, start from bottom up'''
        for i in range(len(self.soillayers)-1, -1, -1):
            if self.soillayers[i][1] <= y <= self.soillayers[i][0]:
                return self.soillayers[i][2]
        return None

    def first_occurence_of(self, soilids):
        '''Returns the distance from the top of the vsoil to the first occurence
        of the given soil.id

        IN: soilids = list of soilids, example [34, 45]
        OUT: distance from top of vsoil to first found soil
        '''
        for s in self.soillayers:
            if s[2] in soilids:
                return self.soillayers[0][0] - s[0]

        return -9999
