#-------------------------------------------------------------------------------
# Name:        storeader
# Purpose:     Reads the results of DGeoStability analysis from sto files
#
# Author:      Rob van Putten
#
# Created:     06-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

class STOReader:
    def __init__(self):
        self.calculation_model = ""
        self.slope_circle = [0.0, 0.0, 0.0] #[x,y,r]
        self.slope_circle_right = [0.0, 0.0] #liftvan only
        self.fmin = 0.0

    def read(self, filename):
        lines = open(filename, 'r').readlines()
        for l in lines:
            if l.find('Calculation model') > -1:
                self.calculation_model = l.split(':')[1].strip()
                if self.calculation_model=="Bishop":
                    self._read_bishop(lines)
                else:
                    self._read_uplift(lines)

    def _read_bishop(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('Information on the critical circle') > -1:
                self.fmin = float(lines[i].split(':')[1].split('=')[1].strip())
                x = float(lines[i+4].split(':')[1].replace('[m]', '').strip())
                y = float(lines[i+5].split(':')[1].replace('[m]', '').strip())
                r = float(lines[i+6].split(':')[1].replace('[m]', '').strip())
                self.slope_circle = [x,y,r]

    def _read_uplift(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('Information on the critical plane') > -1:
                self.fmin = float(lines[i].split(':')[1].split('=')[1][0:15].strip())
                x = float(lines[i+4].split(':')[1].replace('[m]', '').strip())
                y = float(lines[i+5].split(':')[1].replace('[m]', '').strip())
                r = float(lines[i+6].split(':')[1].replace('[m]', '').strip())
                self.slope_circle = [x,y,r]
                x = float(lines[i+8].split(':')[1].replace('[m]', '').strip())
                y = float(lines[i+9].split(':')[1].replace('[m]', '').strip())
                r = float(lines[i+10].split(':')[1].replace('[m]', '').strip())
                self.slope_circle_right = [x,y,r]


if __name__=="__main__":
    s = STOReader()
    s.read('testfile01.sto')
    print s.calculation_model, s.fmin, s.slope_circle
    s.read('testfile02.sto')
    print s.calculation_model, s.fmin, s.slope_circle, s.slope_circle_right
