#-------------------------------------------------------------------------------
# Name:        stireader.py
# Purpose:     Read Deltares STI files (input files for DGeoStability)
#
# Author:      Rob van Putten
#
# Created:     05-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

import bbtools.math.tools
import sys

class STISoil:
    def __init__(self):
        self.name = ""
        self.ydry = 0.0
        self.ysat = 0.0
        self.c = 0.0
        self.phi = 0.0
        self.color = "#ffffff"

class STIReader:
    def __init__(self):
        self.soils = [] #list of STISoil classes
        self.points = [] #list of [id, x, y, z]
        self.curves = [] #list of [id, p1, p2]
        self.boundaries = [] #list of [id, [curve_id_1, curve_id_2 ... curve_id_n]]
        self.pllines = [] #list of [id, [curve_id_1, curve_id_2 ... curve_id_n]]
        self.layers = [] #list of [id, soilname, pl_id_top, pl_id_bottom, boundary_id_top, boundary_id_bottom]
        self.plline = -1

    def read(self, filename):
        lines = open(filename, 'r').readlines()
        self._read_soils(lines)
        self._read_points(lines)
        self._read_curves(lines)
        self._read_boundaries(lines)
        self._read_pllines(lines)
        self._read_layers(lines)
        self._read_extra(lines)

    def _get_boundary_by_id(self, id):
        for b in self.boundaries:
            if b[0] == id:
                return b
        return None

    def _get_curve_by_id(self, id):
        for c in self.curves:
            if c[0] == id:
                return c
        return None

    def _get_point_by_id(self, id):
        for p in self.points:
            if p[0] == id:
                return p
        return None

    def _get_soil_by_name(self, name):
        for s in self.soils:
            if s.name == name:
                return s
        return None

    def _boundary_as_points(self, boundary):
        '''Convert the boundary to a list of [x,y] points'''
        result = []
        for i in range(0, len(boundary[1])):
            curve = self._get_curve_by_id(boundary[1][i])
            p1 = self._get_point_by_id(curve[1])
            result.append([p1[1], p1[2]])
            if i==len(boundary[1])-1:
                p2 = self._get_point_by_id(curve[2])
                result.append([p2[1], p2[2]])
        return result

    def _plline_as_points(self, plline):
        '''Convert the pl line to a list of [x,y] points'''
        result = []
        for i in range(0, len(plline[1])):
            curve = self._get_curve_by_id(plline[1][i])
            p1 = self._get_point_by_id(curve[1])
            result.append([p1[1], p1[2]])
            if i==len(plline[1])-1:
                p2 = self._get_point_by_id(curve[2])
                result.append([p2[1], p2[2]])
        return result

    def _read_extra(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('[PHREATIC LINE]') >-1:
                self.plline = int(lines[i+1].split('-')[0].strip())

    def _read_layers(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('[LAYERS]') >-1:
                num = int(lines[i+1].split('-')[0].strip())
                i += 2
                for j in range(0, num):
                    name = lines[i + j*6 + 1].strip()
                    plidtop = int(lines[i + j*6 + 2].split('-')[0].strip())
                    plidbot = int(lines[i + j*6 + 3].split('-')[0].strip())
                    bdidtop = int(lines[i + j*6 + 4].split('-')[0].strip())
                    bdidbot = int(lines[i + j*6 + 5].split('-')[0].strip())
                    self.layers.append([j+1, name, plidtop, plidbot, bdidtop, bdidbot])

    def _read_pllines(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('[PIEZO LINES]') >-1:
                num = int(lines[i+1].split('-')[0].strip())
                id = 1
                i += 3
                for j in range(0, num):
                    #print lines[i]
                    numcurves = int(lines[i].split('-')[0])
                    iextra = (numcurves-1) / 10

                    pointids = []
                    for m in range(i+1, i+iextra+2):
                        args = lines[m].strip().split(' ')
                        args = [int(a) for a in args if len(a) > 0]
                        for a in args: pointids.append(a)

                    self.pllines.append([j+1, pointids])
                    i += (3 + iextra)

    def _read_boundaries(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('[BOUNDARIES]') >-1:
                num = int(lines[i+1].split('-')[0].strip())
                i += 3
                for j in range(0, num):
                    numcurves = int(lines[i].split('-')[0])
                    iextra = (numcurves-1) / 10
                    pointids = []
                    for m in range(i+1, i+iextra+2):
                        args = lines[m].strip().split(' ')
                        args = [int(a) for a in args if len(a) > 0]
                        for a in args: pointids.append(a)

                    self.boundaries.append([j, pointids])
                    i += (3 + iextra)

    def _read_curves(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('[CURVES]') >-1:
                num = int(lines[i+1].split('-')[0].strip())
                i += 1
                for j in range(0, num):
                    i += 3
                    p1 = int(lines[i][0:10].strip())
                    p2 = int(lines[i][11:16].strip())
                    self.curves.append([j+1,p1,p2])

    def _read_points(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('[POINTS]') >-1:
                num = int(lines[i+1].split('-')[0].strip())
                for i in range(i+2, i+2+num):
                    id = int(lines[i][1:8].strip())
                    x = float(lines[i][9:23].strip())
                    y = float(lines[i][24:38].strip())
                    z = float(lines[i][39:53].strip())
                    self.points.append([id,x,y,z])

    def _read_soils(self, lines):
        for i in range(0, len(lines)):
            if lines[i].find('[SOIL]') >-1:
                soil = STISoil()
                soil.name = lines[i+1].strip()
                i+=2
                while(lines[i].find('[END OF SOIL]')==-1):
                    keyword, arg = lines[i].split('=')
                    if keyword=="SoilColor": soil.color = str(arg.strip())
                    elif keyword=="SoilGamDry": soil.ydry = float(arg.strip())
                    elif keyword=="SoilGamWet": soil.ysat = float(arg.strip())
                    elif keyword=="SoilCohesion": soil.c = float(arg.strip())
                    elif keyword=="SoilPhi": soil.phi = float(arg.strip())
                    i+=1
                self.soils.append(soil)

    def xmin(self):
        '''Returns the minimum x value of all points'''
        result = sys.maxsize
        for p in self.points:
            if p[1] < result:
                result = p[1]
        return result

    def ymin(self):
        '''Returns the minimum y value of all points'''
        result = sys.maxsize
        for p in self.points:
            if p[2] < result:
                result = p[2]
        return result

    def xmax(self):
        '''Returns the maximum x value of all points'''
        result = -sys.maxsize
        for p in self.points:
            if p[1] > result:
                result = p[1]
        return result

    def ymax(self):
        '''Returns the maximum y value of all points'''
        result = -sys.maxsize
        for p in self.points:
            if p[2] > result:
                result = p[2]
        return result

    def get_vertical(self, x, dymax=0.0):
        '''Return a list with intersections of soillayer at point x
        and depth top - dy (if dy=0.0 all intersections are returned)
        Result = list of [ytop, ybottom, soilname]'''
        result = []

        vertline = [x, sys.maxsize, x, -sys.maxsize]
        for l in self.layers:
            btop = self._get_boundary_by_id(l[4])
            bbot = self._get_boundary_by_id(l[5])
            ltop = self._boundary_as_points(btop)
            intersections = bbtools.math.tools.polyline_line_intersection(ltop, vertline)
            xintersection = round(intersections[0][0], 2)
            yintersection = round(intersections[0][1], 2)
            if len(intersections) > 0:
                if len(result) > 0:
                    if (yintersection == result[len(result)-1][1]):
                        result[len(result)-1] = [xintersection, yintersection, l[1]] #replace with the latest layer
                    else:
                        result.append([xintersection, yintersection, l[1]]) #add to list
                else:
                    result.append([xintersection, yintersection, l[1]]) #add to list

        result = sorted(result, key=lambda tup: tup[1], reverse=True)

        if dymax==0.0:
            return result
        else:
            endresult = []
            endresult.append(result[0])
            ytop = result[0][1]
            for i in range(1, len(result)):
                y = result[i][1]
                dy = ytop - y
                if dy > dymax:
                    endresult.append([result[i][0], ytop - dymax, result[i-1][2]])
                    break
                else:
                    endresult.append(result[i])
            return endresult

    def _get_phreaticline(self):
        '''return the waterline acting as the phreatic line'''
        for plline in self.pllines:
            if plline[0] == self.plline:
                return plline
        return None

    def get_phreacticlayer_intersection(self, x):
        '''Return the y coordinatie of the intersection with the phreatic water
        level'''
        pllinepoints = self._plline_as_points(self._get_phreaticline())
        vertline = [x, sys.maxsize, x, -sys.maxsize]
        intersections = bbtools.math.tools.polyline_line_intersection(pllinepoints, vertline)
        xintersection = round(intersections[0][0], 2)
        yintersection = round(intersections[0][1], 2)
        return [xintersection, yintersection]


    def get_soil_statistics(self, x, dy):
        '''Return the following values of the soil under coordinate x from top to top - dy
        [Average cohesion = 0
        Minimum cohesion
        Maximum cohesion
        Average phi
        Minimum phi
        Maximum phi
        Average ydry
        Minimum ydry
        Maximum ydry
        Average ysat
        Minimum ysat
        Maximum ysat
        TODO: Waterlevel from top = 12]'''

        #initialize empty result list
        result = [0.0, sys.maxsize, -sys.maxsize, 0.0, sys.maxsize, -sys.maxsize, 0.0, sys.maxsize, -sys.maxsize, 0.0, sys.maxsize, -sys.maxsize, 0.0]

        #generate vertical and start calculating all the result values
        vertical = self.get_vertical(x, dy)
        for i in range(0, len(vertical)-1):
            ytop = vertical[i][1]
            ybot = vertical[i+1][1]
            soil = self._get_soil_by_name(vertical[i][2])
            if soil==None:
                print "Unexpected error while trying to get a soil by name."
                sys.exit(1)
            result[0] += soil.c * (ytop - ybot) / dy
            if soil.c < result[1]: result[1] = soil.c
            if soil.c > result[2]: result[2] = soil.c
            result[3] += soil.phi * (ytop - ybot) / dy
            if soil.phi < result[4]: result[4] = soil.phi
            if soil.phi > result[5]: result[5] = soil.phi
            result[6] += soil.ydry * (ytop - ybot) / dy
            if soil.ydry < result[7]: result[7] = soil.ydry
            if soil.ydry > result[8]: result[8] = soil.ydry
            result[9] += soil.ysat * (ytop - ybot) / dy
            if soil.ysat < result[10]: result[10] = soil.ysat
            if soil.ysat > result[11]: result[11] = soil.ysat

        #round values
        result[0] = round(result[0], 2)
        result[3] = round(result[3], 2)
        result[6] = round(result[6], 2)
        result[9] = round(result[9], 2)
        #now add the intersection with the phreatic line
        result[12] = round(vertical[0][1] - self.get_phreacticlayer_intersection(x)[1], 2)
        return result

if __name__=="__main__":
    s = STIReader()
    s.read('testfile.sti')
    print s.get_soil_statistics(35, 5)





