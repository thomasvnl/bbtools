#-------------------------------------------------------------------------------
# Name:        metreader.py
# Purpose:     read met file content and store in a convenient way
#              expects default met files Waternet-style (dike id like
#              CODE_METRERING
#              90 = ref lijn
#
#
# Author:      Rob van Putten
#
# Created:     06-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

import bbtools.crosssections.crosssection

class METReader:
    def __init__(self):
        self.css = []

    def read(self, filename):
        lines = open(filename, 'r').readlines()
        for i in range(0, len(lines)):
            if lines[i].find('<PROFIEL>') > -1:
                args = lines[i].split(',')
                dpname = args[0].replace('<PROFIEL>', '')
                dp = bbtools.crosssections.crosssection.Crosssection()
                dp.code = dpname.split('_')[0]
                dp.metrering = int(dpname.split('_')[1])
                i+=1
                while(lines[i].find('</PROFIEL>') == -1):
                    args = lines[i].split(',')
                    dp.points.append([int(args[0].replace('<METING>', '')), float(args[2]), float(args[3]), float(args[4])])
                    i+=1
                    if i>=len(lines): #extra way out in case </PROFIEL> is missing
                        break
                self.css.append(dp)

    def get_by_metrering(self, metrering):
        for c in self.css:
            if c.metrering == metrering:
                return c
        return None

if __name__ == "__main__":
    m = METReader()
    m.read("testfile.met")
    m.get_by_metrering(100)




