#-------------------------------------------------------------------------------
# Name:        abcmodel
# Purpose:     Calculate settlement based on the abc / isotache model
#              based on `Introduction to soft soil geotechnique' by
#              Frans Barends
#
# Author:      Rob van Putten
#
# Created:     07-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

import math
import bbtools.calc.soilstresses
import bbtools.objects.soiltype

REF_TIME = 1

def calculate_settlement(vsoil, waterlevel, load, preconsolidation_load, creep_period_in_days, soiltypes):
    '''Calculates the settlement based on the abc / isotache method
    Returns [total settlement, creep settlement]'''
    input = [] #[e0, a, b, c, sa, u, saa, sab, sac]
    output = [] #[eps1, stotal, screep]
    sbefore = bbtools.calc.soilstresses.calc_soil_stresses_in_vsoil(vsoil, waterlevel, 0.0, soiltypes) #without load
    #safter = bbtools.calc.soilstresses.calc_soil_stresses_in_vsoil(vsoil, waterlevel, load, soiltypes) #with load

    for s in sbefore:
        soilname = vsoil.get_soilname_at(s[0])
        e0 = 0.0
        a = 0.0
        b = 0.0
        c = 0.0
        found = False
        for st in soiltypes:
            if st.name == soilname:
                e0 = st.e0
                a = st.a_abc
                b = st.b_abc
                c = st.c_abc
                found = True
                break
        #check if the soiltype is found
        if not found:
            print "[E] Fout in invoer, onbekende grondsoort tegengekomen"
            print "[E] Verticaal data: " + vsoil.data
            print "[E] Onbekende grondsoort: " + sl[2]

        #             0  1   2  3  4   5     6     7     8            9
        input.append([s[0], e0, a, b, c, s[1], s[2], s[3], s[3] + preconsolidation_load, s[3] + load])

    #start calculation
    stotal = 0.0
    screep = 0.0
    for i in range(0, len(input)-1):
        if i==0:
            dy = input[0][0] - input[1][0]
            siga = (input[0][7] + input[1][7]) / 2.0
            sigb = (input[0][8] + input[1][8]) / 2.0
            sigc = (input[0][9] + input[1][9]) / 2.0
            eps1 = 1 - math.pow((sigb / siga), -input[0][2]) * math.pow((sigc / sigb), -input[0][3]) * math.pow((creep_period_in_days / REF_TIME), -input[0][4])
            stotal += dy * eps1
            screep += dy * (math.pow(sigb/siga, -input[0][2]) * math.pow(sigc/sigb, -input[0][3])*(1-math.pow(creep_period_in_days / REF_TIME, -input[0][4])))
        else:
            dy = input[i][0] - input[i+1][0]
            sigatop = input[i][7]
            sigabot = input[i+1][7]
            sigbtop = input[i][8]
            sigbbot = input[i+1][8]
            sigctop = input[i][9]
            sigcbot = input[i+1][9]
            epstop = 1 - math.pow((sigbtop / sigatop), -input[i][2]) * math.pow((sigctop / sigbtop), -input[i][3]) * math.pow((creep_period_in_days / 1), -input[i][4])
            epsbot = 1 - math.pow((sigbbot / sigabot), -input[i][2]) * math.pow((sigcbot / sigbbot), -input[i][3]) * math.pow((creep_period_in_days / 1), -input[i][4])
            stotal += dy * (epsbot + epstop) / 2.0
            screep += dy * (math.pow(sigbtop/sigatop, -input[i][2]) * math.pow(sigctop/sigbtop, -input[i][3])*(1-math.pow(creep_period_in_days / REF_TIME, -input[i][4])))

    return [stotal, screep]

#if __name__=="__main__":
#    soiltypes = []
#    s1 = bbtools.objects.soiltype.Soiltype()
#    s2 = bbtools.objects.soiltype.Soiltype()
#    s3 = bbtools.objects.soiltype.Soiltype()
#    s1.name = "1"
#    s1.e0 = 0.667
#    s1.ydry = 16
#    s1.ysat = 20
#    s1.a_abc = 0.0003
#    s1.b_abc = 0.0016
#    s1.c_abc = 0
#    s2.name = "2"
#    s2.e0 = 0.724
#    s2.ydry = 12.8
#    s2.ysat = 17.0
#    s2.a_abc = 0.0303
#    s2.b_abc = 0.1917
#    s2.c_abc = 0.0013
#    s3.name = "3"
#    s3.e0 = 0.538
#    s3.ydry = 14.0
#    s3.ysat = 18.0
#    s3.a_abc = 0.0113
#    s3.b_abc = 0.0395
#    s3.c_abc = 0.0006
#    soiltypes.append(s1)
#    soiltypes.append(s2)
#    soiltypes.append(s3)

#    import bbtools.objects.vsoil
#    vs = bbtools.objects.vsoil.VSoil()
#    vs.soillayers.append([0, -1.0, '1'])
#    vs.soillayers.append([-1.0, -2.0, '2'])
#    vs.soillayers.append([-2.0, -7.0, '3'])
#    print calculate_settlement(vs, -1.0, 100, 50, 27*365, soiltypes)
#    vs2 = bbtools.objects.vsoil.VSoil()
#    vs2.soillayers.append([0, -1.0, '1'])
#    vs2.soillayers.append([-1.0, -6.0, '3'])
#    vs2.soillayers.append([-6.0, -7.0, '2'])
#    print calculate_settlement(vs2, -1.0, 100, 50, 27*365, soiltypes)


