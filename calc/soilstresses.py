#-------------------------------------------------------------------------------
# Name:        soilstresses
# Purpose:     Calculates soilstresses from vsoils
#
# Author:      Rob van Putten
#
# Created:     07-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

WEIGTH_WATER = 10.0

def calc_soil_stresses_in_vsoil(vsoil, waterlevel, load, soiltypes):
    '''Calculate the stresses in a vsoil, returns an array of
    values [depth, total stress, waterpressure, eff stress]'''
    stresses = []
    s = 0.0 #sigma
    u = 0.0 #water

    #water above top of soillayers?
    if waterlevel > vsoil.soillayers[0][0]:
        stresses.append([vsoil.soillayers[0][2], waterlevel, 0.0, 0.0, 0.0])
        u = (waterlevel - vsoil.soillayers[0][0]) * WEIGTH_WATER
        s = u + load #strange but not impossible
        stresses.append([vsoil.soillayers[0][2], waterlevel, s, u, s-u])
    else:
        s = load
        stresses.append([vsoil.soillayers[0][0], s, u, s-u])

    for sl in vsoil.soillayers:
        yd = 0.0
        ys = 0.0
        found = False
        for st in soiltypes:
            if st.name == sl[2]:
                yd = st.ydry
                ys = st.ysat
                found = True
                break
        #check if the soiltype is found
        if not found:
            print "[E] Fout in invoer, onbekende grondsoort tegengekomen"
            print "[E] Verticaal data: " + vsoil.data
            print "[E] Onbekende grondsoort: " + sl[2]

        if waterlevel >= sl[0]: #use ysat
            u = (waterlevel - sl[1]) * WEIGTH_WATER
            s += (sl[0] - sl[1]) * ys
            stresses.append([sl[1], s, u, s-u])
        elif waterlevel <= sl[1]: #use ydry
            u = 0.0
            s += (sl[0] - sl[1]) * yd
            stresses.append([sl[1], s, u, s-u])
        else: #use yd and ys
            s += (sl[0] - waterlevel) * yd
            stresses.append([waterlevel, s, u, s-u])
            s += (waterlevel - sl[1]) * ys
            u = (waterlevel - sl[1]) * WEIGTH_WATER
            stresses.append([sl[1], s, u, s-u])

    return stresses

if __name__=="__main__":
    import bbtools.db.subsoil
    st = bbtools.db.subsoil.get_all_soiltypes()
    vs = bbtools.db.subsoil.get_all_vsoils()
    print calc_soil_stresses_in_vsoil(vs[0], 2.2, 0.0, st)


