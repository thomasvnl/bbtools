#-------------------------------------------------------------------------------
# Name:        labtestst
# Purpose:     Communication between apps and labtest database
#
# Author:      Rob van Putten
#
# Created:     13-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

import sys
import sqlite3, bbtools.objects.vsoil, bbtools.objects.triaxialtest
from inspect import getmembers

DATABASEFILE = "d:\\Programmeren\\testdata\\labtests.sqlite"

def get_all_triaxial_tests():
    '''Select all data from triaxial and save them in a list of triaxialtest classes'''
    triaxtest = []
    con = sqlite3.connect(DATABASEFILE)
    con.row_factory = sqlite3.Row #use row by string
    cur = con.cursor()
    cur.execute("SELECT * FROM triaxial")
    rows = cur.fetchall()
    #get the column names from the database
    names = list(map(lambda x: x[0], cur.description))
    #get the member names of the classes
    ttmembers = getmembers(bbtools.objects.triaxialtest.TriaxialTest())

    for r in rows:
        tt = bbtools.objects.triaxialtest.TriaxialTest()
        #try to find a match between the column name (n) and the class members
        #(m[0]), if a match is found use setattr to set the value of the class
        #member, this way we avoid a lot of stupid conversions
        for n in names:
            for m in ttmembers:
                if m[0] == n:
                    setattr(tt, n, r[m[0]])
        triaxtest.append(tt)
    return triaxtest
