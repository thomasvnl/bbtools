#-------------------------------------------------------------------------------
# Name:        subsoil
# Purpose:     Communication between python modules and the sqlite database
#              containing subsoil information
#
# Author:      Breinbaas
#
# Created:     22-12-2014
# Copyright:   (c) Breinbaas 2014
# Licence:     GPL
#-------------------------------------------------------------------------------

import sys
import sqlite3, bbtools.objects.vsoil, bbtools.objects.soiltype
from inspect import getmembers

DATABASEFILE = "d:\\Programmeren\\testdata\\ondergrond.sqlite"

def get_all_peat_soilids():
    '''Return list with soilids containing peat'''
    peatnames = ['veen', 'hv', 'bv', 'agv'] #these are peat identifiers in the database
    peatids = []
    con = sqlite3.connect(DATABASEFILE)
    cur = con.cursor()
    cur.execute("SELECT id, name FROM soiltypes")
    rows = cur.fetchall()
    for r in rows:
        for p in peatnames:
            if r[1].lower().find(p) > -1:
                peatids.append(r[0])
                break
    return peatids

def get_all_soiltypes():
    '''Select all data from soiltypes and save them in a list of soiltype classes'''
    soiltypes = []
    con = sqlite3.connect(DATABASEFILE)
    con.row_factory = sqlite3.Row #use row by string
    cur = con.cursor()
    cur.execute("SELECT * FROM soiltypes")
    rows = cur.fetchall()
    #get the column names from the database
    names = list(map(lambda x: x[0], cur.description))
    #get the member names of the classes
    stmembers = getmembers(bbtools.objects.soiltype.Soiltype())

    for r in rows:
        st = bbtools.objects.soiltype.Soiltype()
        #try to find a match between the column name (n) and the class members
        #(m[0]), if a match is found use setattr to set the value of the class
        #member, this way we avoid a lot of stupid conversions
        for n in names:
            for m in stmembers:
                if m[0] == n:
                    setattr(st, n, r[m[0]])
        soiltypes.append(st)
    return soiltypes

def get_all_vsoils():
    '''Select all data from vsoils and save them in a list of vsoil classes'''
    vsoils = []
    con = sqlite3.connect(DATABASEFILE)
    con.row_factory = sqlite3.Row #we use row by string
    cur = con.cursor()
    cur.execute("SELECT * FROM vsoils")
    rows = cur.fetchall()
    #get the column names from the database
    names = list(map(lambda x: x[0], cur.description))
    #get the member names of the classes
    vsmembers = getmembers(bbtools.objects.vsoil.VSoil())

    for r in rows:
        try:
            v = bbtools.objects.vsoil.VSoil()
            #try to find a match between the column name (n) and the class members
            #(m[0]), if a match is found use setattr to set the value of the class
            #member, this way we avoid a lot of stupid conversions
            for n in names:
                for m in vsmembers:
                    if m[0] == n:
                        setattr(v, n, r[m[0]])

            #convert the data to a more convenient soillayers layout (directly
            #compatible with other modules (like calc)
            args = str(r['data']).split(';')
            v.soillayers.append([float(args[0]), float(args[1]), args[2]])
            prevz = float(args[1])
            for i in range(3, len(args)-1, 2):
                v.soillayers.append([prevz, float(args[i]), args[i+1]])
                prevz = float(args[i])

            #add the vsoil to the result
            vsoils.append(v)
        except:
            print '[E] Error reading vsoils'
            print '[E] At index: %d' % r['id']
            print '[E] arguments in vsoil data: ' + r['data']
            sys.exit(1)

    return vsoils




