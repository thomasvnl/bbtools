#-------------------------------------------------------------------------------
# Name:        tools.py
# Purpose:
#
# Author:      Rob van Putten
#
# Created:     05-01-2015
# Copyright:   (c) Rob van Putten 2015
# Licence:     GPL
#-------------------------------------------------------------------------------

def line_length(x1, y1, x2, y2):
    """Simple Pythagoras calculation to calculate line length,
    RETURNS length of the line"""
    dx = x2 - x1
    dy = y2 - y1
    return math.sqrt(dx*dx + dy*dy)

def circle_polyline_intersection(l, c):
    """Calculate all intersections of a polyline l and a circle c
    format l = [[x1,y1],[x2,y2],..,[xn,yn]]
    format c = [mx, my, r] (mid point + radius circle)
    RETURNS list of intersection points (if any)"""
    result = []
    for i in range(0, len(l)-1):
        mx, my, r = c

        x1, y1 = l[i]
        x2, y2 = l[i+1]

        dx = x2 - x1
        dy = y2 - y1
        A = dx*dx + dy*dy
        B = 2 * (dx * (x1-mx) + dy * (y1-my))
        C = (x1-mx) * (x1-mx) + (y1-my) * (y1-my) - r*r
        det = B*B - 4*A*C
        if (A >= 1e-9 and det >= 0):
            t = (-B - math.sqrt(det)) / (2*A)
            x = x1 + t*dx
            y = y1 + t*dy
            if (x>=x1 and x<x2):
                result.append([x,y])

        if(det > 0):
            t = (-B + math.sqrt(det)) / (2*A)
            x = x1 + t*dx
            y = y1 + t*dy
            if (x>=x1 and x<x2):
                result.append([x,y])

    #TODO, sort on distance
    return result

def polyline_line_intersection(pl, l):
    """Calculate the intersections of a line l [x1, y1, x2, y2] with a polyline pl
    format pl = [[x1,y1],[x2,y2],...,[xn,yn]]
    RETURNS list of intersection points (if any)"""
    def line_from_points(x1, y1, x2, y2):
        """Inline function to write two vectors as y=ax+b"""
        A = y2-y1
        B = x1-x2
        C = A*x1 + B*y1
        return A,B,C

    result = []
    x1,y1,x2,y2 = l

    for i in range(0, len(pl)-1):
        x3, y3 = pl[i]
        x4, y4 = pl[i+1]

        A1,B1,C1 = line_from_points(x1, y1, x2, y2)
        A2,B2,C2 = line_from_points(x3, y3, x4, y4)
        det = A1*B2 - A2*B1

        if det != 0:
            x = (B2*C1 - B1*C2) / det
            y = (A1*C2 - A2*C1) / det

            if x>=x3 and x<=x4:
                result.append([x,y])
    return result

